# pesto
---
pesto is a python module consisting of **p**hoto**e**mission **s**pectroscopy **to**ols. It provides functions for loading, visualizing and analyzing photoemission data acquired from the BLOCH beamline'at the MAX-IV synchrotron facility. It is intended to be used within a Jupyter notebook environment.

**Author:** Craig Polley (craig.polley@maxiv.lu.se)

**Version:** 1.1.1

Documentation: https://pesto.readthedocs.io/


