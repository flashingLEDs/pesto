Notes on pesto 'spectrum' dictionaries
======================================

A 'spectrum' in pesto is a dictionary object containing all information about the measurement. 
In terms of interaction with other pesto functions, the mandatory components of a 'spectrum' dictionary are:

1. ['Metadata'] dictionary (but it can be empty)
2. ['data'] 
3. ['Axis'] 
4. ['AxisLabel']
5. ['AxisUnits']

Pesto attempts to infer a lot from this information, particularly for 'catchall' functions that try to always do the right thing regardless of what the input is, like quickPlot, explorer or kwarp.

To see the structure of a given spectrum, call pesto.tree() on it:

.. image:: /_static/pesto/tree.png
	:align: center


Special case: Spin ARPES
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A spinARPES measurement at Bloch typically consists of repeated measurements of the 0-D channeltron detector, using different coil polarities. Angles or energies may be swept to obtain 1D (EDC,MDC) or 2D (E-k, k-k) datasets. 

The different coil polarities are handled as an extra dimension in the data, e.g. an EDC with 8 magnetization cycles has the intensity data stored in a 2D array, where one axis is kinetic energy and the other is 'scan number'/'magnetization cycle'.

To make sense of this, we need to keep track of what the target magnetization was for each 'magnetization cycle'. This information is stored in ['Metadata']['CoilPolarity']



------------




