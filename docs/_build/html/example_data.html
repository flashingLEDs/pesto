

<!DOCTYPE html>
<html class="writer-html5" lang="en" data-content_root="./">
<head>
  <meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Example data from Bloch &mdash; BlochDocs  documentation</title>
      <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=03e43079" />
      <link rel="stylesheet" type="text/css" href="_static/css/theme.css?v=7ab3649f" />
      <link rel="stylesheet" type="text/css" href="_static/css/custom.css?v=7dc6ce7e" />

  
      <script src="_static/jquery.js?v=5d32c60e"></script>
      <script src="_static/_sphinx_javascript_frameworks_compat.js?v=2cd50e6c"></script>
      <script src="_static/documentation_options.js?v=5929fcd5"></script>
      <script src="_static/doctools.js?v=9a2dae69"></script>
      <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
      <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="_static/js/theme.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search"  style="background: white" >

          
          
          <a href="index.html" class="icon icon-home">
            BlochDocs
          </a>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" aria-label="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <p class="caption" role="heading"><span class="caption-text">pesto</span></p>
<ul>
<li class="toctree-l1"><a class="reference internal" href="pesto_settingup.html">Setting up</a></li>
<li class="toctree-l1"><a class="reference internal" href="pesto_preliminarynotes.html">Notes on pesto ‘spectrum’ dictionaries</a></li>
<li class="toctree-l1"><a class="reference internal" href="pesto_bloch.html">Bloch-specific calculators</a></li>
<li class="toctree-l1"><a class="reference internal" href="pesto_core.html">Core functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="pesto_export.html">Spectrum export functions</a></li>
<li class="toctree-l1"><a class="reference internal" href="pesto_spin.html">spin module</a></li>
<li class="toctree-l1"><a class="reference internal" href="pesto_XPS.html">XPS module</a></li>
<li class="toctree-l1"><a class="reference internal" href="pesto_DFT.html">DFT module</a></li>
<li class="toctree-l1"><a class="reference internal" href="snippets.html">Code snippets and examples</a></li>
<li class="toctree-l1"><a class="reference internal" href="kwarping.html">Notes about k-warping</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu"  style="background: white" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="index.html">BlochDocs</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="index.html" class="icon icon-home" aria-label="Home"></a></li>
      <li class="breadcrumb-item active">Example data from Bloch</li>
      <li class="wy-breadcrumbs-aside">
            <a href="_sources/example_data.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <section id="example-data-from-bloch">
<h1>Example data from Bloch<a class="headerlink" href="#example-data-from-bloch" title="Link to this heading"></a></h1>
<p>While <em>pesto</em> outwardly presents only a single function to k-warp data (i.e. to map angles to wavevectors), behind the scenes it is looking at what you have passed in and choosing one of four different k-warping functions to apply. This page contains an explanation of what is going on in each of them.</p>
<section id="simple-case">
<h2>Simple case<a class="headerlink" href="#simple-case" title="Link to this heading"></a></h2>
<p>The simplest case is one where the analyzer slit is centered at normal emission, i.e. no polar or tilt offset. The analyzer records electrons emitted within the plane of the analyzer slit, and tells us the angle <span class="math notranslate nohighlight">\(\alpha\)</span> at which they were emitted.</p>
<p>The in-plane component of the electron wavevector, at least in the direction of the analyzer slit (<span class="math notranslate nohighlight">\(k_Y\)</span>) can be determined from such a measurement:</p>
<img alt="_images/kwarp_geometry.png" class="align-center" src="_images/kwarp_geometry.png" />
<div class="math notranslate nohighlight">
\[k_Y = |\vec{k}| \sin(\alpha)\]</div>
<div class="math notranslate nohighlight">
\[E_K = |\vec{p}|^2/2m_e\]</div>
<div class="math notranslate nohighlight">
\[|\vec{p}| = \hbar |\vec{k}|\]</div>
<p>Combining, we get:</p>
<div class="math notranslate nohighlight">
\[ \begin{align}\begin{aligned}k_Y = 1/\hbar  \sqrt{2  m_e  E_k}  \sin(\alpha)\\        = 0.512 \sqrt{E_k}  \sin(\alpha)\end{aligned}\end{align} \]</div>
<p>The magic number 0.512 comes from the term <span class="math notranslate nohighlight">\(\frac{\sqrt{2 m_e}}{ \hbar}\)</span>:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">eV_per_J</span> <span class="o">=</span> <span class="mf">1.602176e-19</span>
<span class="n">hbar__Js</span> <span class="o">=</span> <span class="mf">1.054571e-34</span>
<span class="n">electron_mass__kg</span> <span class="o">=</span> <span class="mf">9.1093e-31</span>
<span class="n">angstroms_per_meter</span><span class="o">=</span><span class="mf">1e-10</span>

<span class="n">prefactor</span> <span class="o">=</span> <span class="p">(</span><span class="n">math</span><span class="o">.</span><span class="n">sqrt</span><span class="p">(</span><span class="mi">2</span><span class="o">*</span><span class="n">electron_mass__kg</span><span class="o">*</span><span class="n">eV_per_J</span><span class="p">)</span><span class="o">/</span><span class="p">(</span><span class="n">hbar__Js</span><span class="p">))</span><span class="o">*</span><span class="n">angstroms_per_meter</span> <span class="c1"># = 0.51234</span>
</pre></div>
</div>
<p>Things remain simple if we introduce a tilt angle offset <span class="math notranslate nohighlight">\(\xi\)</span> to the emission angle <span class="math notranslate nohighlight">\(\alpha\)</span>:</p>
<div class="math notranslate nohighlight">
\[k_Y = 0.512 \sqrt{E_k} \sin(\alpha + \xi)\]</div>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>An important observation is that k-warping single <span class="math notranslate nohighlight">\(E_K\)</span>-vs-<span class="math notranslate nohighlight">\(\alpha\)</span> images has the effect of keystone distorting the image - i.e. squashing it at the bottom. A given angle range spans a smaller k range at lower kinetic energies. The origin of this is the  <span class="math notranslate nohighlight">\(\sqrt{E_K}\)</span> term in the total photoelectron momentum  <span class="math notranslate nohighlight">\(|\vec{k}|\)</span>. As a consequence, the effect is more obvious when measuring lower kinetic energies and when spanning a large <span class="math notranslate nohighlight">\(E_K\)</span> range.</p>
</div>
<p>Within <em>pesto</em>, this operation on 2D images is handled by the internal function <code class="docutils literal notranslate"><span class="pre">kwarp__energyAngleImage()</span></code>. It performs a reverse interpolation: construct the energy-k grid we want to have, and then point-by-point look up the intensity of the corresponding pixel(s) in the source energy-angle image.</p>
<p>Photon energy scan 3D datasets are k-warped in the same way, by the internal <em>pesto</em> function <code class="docutils literal notranslate"><span class="pre">kwarp__wholeMatrix_hvscan_warp()</span></code>. This simple treatment is possible because at least so far <em>pesto</em> only warps the analyzer angle axis, it does not warp <span class="math notranslate nohighlight">\(h\nu\)</span> to <span class="math notranslate nohighlight">\(k_Z\)</span></p>
<p>Since the slit is centered at normal emission, the detected electrons will have no component in the perpendicular (<span class="math notranslate nohighlight">\(k_X\)</span>) direction.</p>
</section>
<section id="warping-with-a-polar-angle-or-deflection-offset">
<h2>Warping with a polar angle or deflection offset<a class="headerlink" href="#warping-with-a-polar-angle-or-deflection-offset" title="Link to this heading"></a></h2>
<p>If we have rotated the sample in the polar direction (perpendicular to the analyzer slit) away from normal emission, OR deflected off-normal electrons into the analyzer, we now have the problem that electrons detected by the analyzer will have both <span class="math notranslate nohighlight">\(k_X\)</span> and <span class="math notranslate nohighlight">\(k_Y\)</span> components. This is treatable with some heavy trigonometry - for details, see  <a class="reference external" href="https://doi.org/10.1063/1.5007226">Ishida and Shin</a> (Rev. Sci. Inst. <strong>89</strong> 043903 2018). Their ‘Type I’ and ‘Type I+deflector’ systems match the geometry at Bloch, but we slightly alter their equations in the following ways.</p>
<p>The geometry at Bloch, applicable for both endstations, is shown below:</p>
<img alt="_images/blochGeometry.png" class="align-center" src="_images/blochGeometry.png" />
<p>We choose to have a system in which positive analyzer slit angles correspond to electrons with positive <span class="math notranslate nohighlight">\(k_Y\)</span>, and going to positive manipulator polar angles probes electrons with positive <span class="math notranslate nohighlight">\(k_X\)</span>. On the A-endstation, going to negative tilt angles probes electrons with more negative <span class="math notranslate nohighlight">\(k_Y\)</span> values, while going to positive deflector angles probes electrons with positive <span class="math notranslate nohighlight">\(k_X\)</span>.</p>
<p>In the coordinate system described by Ishida and Shin, if we set a constant azimuthal angle (<span class="math notranslate nohighlight">\(\delta\)</span>) of <span class="math notranslate nohighlight">\(\pi/2\)</span> then we achieve almost all of this, we are just left with positive analyzer angles mapping to negative <span class="math notranslate nohighlight">\(k_Y\)</span> values. This is addressed by inverting <span class="math notranslate nohighlight">\(\alpha\)</span> whenever it appears.</p>
<p>Setting this fixed azimuth also sets all <span class="math notranslate nohighlight">\(\sin(\delta)\)</span> terms to 1 and all <span class="math notranslate nohighlight">\(\cos(\delta)\)</span> terms to 0, considerably simplifying the warping equations and speeding up the calculations. While it removes the ability to rotate the data <em>during</em> k-warping, this can be handled independently after performing k-warping if required.</p>
<p>Finally, Ishida and Shin redefine <span class="math notranslate nohighlight">\(\beta\)</span> between the manipulator (<span class="math notranslate nohighlight">\(f_I\)</span>) and deflector (<span class="math notranslate nohighlight">\(f_I'\)</span>) cases. For consistency we choose <span class="math notranslate nohighlight">\(\beta\)</span> to always mean deflection perpendicular to the analyzer slit, and <span class="math notranslate nohighlight">\(\chi\)</span> to always mean the sample polar angle offset.</p>
<p>With all that said, let:</p>
<p><span class="math notranslate nohighlight">\(\chi\)</span> = polar angle offset of the sample (perpendicular to the analyzer slit)</p>
<p><span class="math notranslate nohighlight">\(\xi\)</span> = tilt angle offset of the sample (along the analyzer slit)</p>
<p><span class="math notranslate nohighlight">\(\alpha\)</span> = emission angle recorded by the analyzer, along the slit</p>
<p><span class="math notranslate nohighlight">\(\beta\)</span> = emission angle recorded by the analyzer when deflecting, perpendicular to the slit</p>
<p><strong>Mechanical</strong> polar-scan 3D datasets are k-warped by the internal <em>pesto</em> function <code class="docutils literal notranslate"><span class="pre">kwarp__wholeMatrix_kwarp()</span></code>, and use the following equations for forward and reverse mapping:</p>
<div class="math notranslate nohighlight">
\[k_X = C \left(\sin(\chi)\cos(-\alpha)\right)\]</div>
<div class="math notranslate nohighlight">
\[k_Y = C \left(\cos(-\alpha)\sin(\xi)\cos(\chi) - \cos(\xi)\sin(-\alpha)\right)\]</div>
<div class="math notranslate nohighlight">
\[\alpha = -\textrm{asin}\left(\left(\sin(\xi) E - \cos(\xi)k_Y  \right)/C\right)\]</div>
<div class="math notranslate nohighlight">
\[\chi = \textrm{atan}\left((k_X) / \left(\sin(\xi)k_Y + \cos(\xi)\left( E \right)\right)\right)\]</div>
<p>where:</p>
<p><span class="math notranslate nohighlight">\(C = (0.512316\sqrt{E_K})\)</span></p>
<p><span class="math notranslate nohighlight">\(E = \sqrt{C^2 - k_X^2 - k_Y^2}\)</span></p>
<p>Corresponding functions in <em>pesto</em>: <code class="docutils literal notranslate"><span class="pre">angle_to_kx_manipulator()</span></code>, <code class="docutils literal notranslate"><span class="pre">angle_to_ky_manipulator()</span></code>, <code class="docutils literal notranslate"><span class="pre">k_to_polar_manipulator()</span></code></p>
<p><strong>Deflector-scan</strong> 3D datasets are performed by <code class="docutils literal notranslate"><span class="pre">kwarp__wholeMatrix_kwarp_deflector()</span></code>, and use the following horrific equations for forward and reverse mapping:</p>
<div class="math notranslate nohighlight">
\[k_X = C \left(\beta\cos(\chi)\frac{\sin(\sqrt{\alpha^2 + \beta^2})}{\sqrt{\alpha^2 + \beta^2}} +   \sin(\chi)\cos(\sqrt{\alpha^2 + \beta^2}) \right)\]</div>
<div class="math notranslate nohighlight">
\[k_Y = C \left(\left(\alpha\cos(\xi) - \beta\sin(\xi)\sin(\chi) \right)\frac{\sin(\sqrt{\alpha^2 + \beta^2})}{\sqrt{\alpha^2 + \beta^2}} +   \left(\sin(\xi)\cos(\chi)\right)\cos(\sqrt{\alpha^2 + \beta^2}) \right)\]</div>
<div class="math notranslate nohighlight">
\[\begin{split}\alpha = \frac{\textrm{acos}\left(\left(\sin(\chi)k_X + \cos(\chi)\sin(\xi)k_y + \cos(\chi)\cos(\xi)E\right)/C \right) \\ \times \left(\cos(\xi)k_Y - \sin(\xi)E\right)}{\sqrt{C^2 - \left(\sin(\chi)k_x + \cos(\chi)\sin(\xi)k_y + \cos(\chi)\cos(\xi)E \right)^2}}\end{split}\]</div>
<div class="math notranslate nohighlight">
\[\begin{split}\beta = -\frac{\textrm{acos}\left(\left(\sin(\chi)k_X + \cos(\chi)\sin(\xi)k_y + \cos(\chi)\cos(\xi)E\right)/C \right) \\ \times \left(-\cos(\chi)k_x + \sin(\chi)\sin(\xi)k_Y + \sin(\chi)\cos(\xi)E\right)}{\sqrt{C^2 - \left(\sin(\chi)k_x + \cos(\chi)\sin(\xi)k_y + \cos(\chi)\cos(\xi)E \right)^2}}\end{split}\]</div>
<p>Corresponding functions in <em>pesto</em>: <code class="docutils literal notranslate"><span class="pre">None</span></code>, <code class="docutils literal notranslate"><span class="pre">angle_to_ky_deflector()</span></code>, <code class="docutils literal notranslate"><span class="pre">k_to_alpha_deflector()</span></code>, <code class="docutils literal notranslate"><span class="pre">k_to_beta_deflector()</span></code></p>
<hr class="docutils" />
<p>As a quick sanity check, notice that in both the deflector and manipulator scan cases, if we set polar <span class="math notranslate nohighlight">\(\chi\)</span> and deflection <span class="math notranslate nohighlight">\(\beta\)</span> angles to zero, the expression for <span class="math notranslate nohighlight">\(k_Y\)</span> reduces to:</p>
<div class="math notranslate nohighlight">
\[k_Y = 0.512\sqrt{E_K} \left(\cos(-\alpha)\sin(\xi) - \cos(\xi)\sin(-\alpha)\right)\]</div>
<div class="math notranslate nohighlight">
\[k_Y = 0.512\sqrt{E_K} \left(\cos(\alpha)\sin(\xi) + \cos(\xi)\sin(\alpha)\right)\]</div>
<p>which by a trig identity reduces to the simple equation we were using earlier:</p>
<div class="math notranslate nohighlight">
\[k_Y = 0.512 \sqrt{E_k} \sin(\alpha + \xi)\]</div>
</section>
<section id="why-are-different-equations-necessary-for-mechanical-vs-deflection-arpes-maps">
<h2>Why are different equations necessary for mechanical vs deflection ARPES maps?<a class="headerlink" href="#why-are-different-equations-necessary-for-mechanical-vs-deflection-arpes-maps" title="Link to this heading"></a></h2>
<p>You can acquire a constant energy surface by either scanning <span class="math notranslate nohighlight">\(\beta\)</span> in a deflector measurement or scanning <span class="math notranslate nohighlight">\(\chi\)</span> in a mechanical polar scan. The k-warping of the datasets diverges whenever <strong>both</strong> <span class="math notranslate nohighlight">\(\alpha\)</span> and <span class="math notranslate nohighlight">\(\beta\)</span> in the deflector measurement are non-zero. This is because there are different coordinate systems involved in each case. Conventional ARPES mapping employs a <em>tilt-angular</em> coordinate system, see (b) below, like latitude and longitude in geography. Deflector schemes employ a different scheme called <em>tilt-polar</em>, see (a) below (figure reproduced from Ishida and Shin). What we are calling <span class="math notranslate nohighlight">\(\alpha\)</span> and <span class="math notranslate nohighlight">\(\beta\)</span> for the deflector equations correspond to <span class="math notranslate nohighlight">\(\Theta_X\)</span> and <span class="math notranslate nohighlight">\(\Theta_Y\)</span> in this figure.</p>
<img alt="_images/kwarp_coordinatesystems.png" class="align-center" src="_images/kwarp_coordinatesystems.png" />
<p>The tilt-angular coordinate system has the property that iso-tilt lines get ‘squeezed together’ at the poles (or if you prefer, latitude spacing decreases at high longitudes). The same is not true in the <span class="math notranslate nohighlight">\(\Theta_X\)</span>-<span class="math notranslate nohighlight">\(\Theta_Y\)</span> system. In practice, with mainstream hemispherical analysers the error introduced by using the wrong k-warping equations is quite small, 1-2% at most. This is because the differences only grow pronounced at large angles, which are never reached when the angular acceptance is limited to around ± 15°.</p>
</section>
<section id="the-problem-of-warping-single-ek-vs-angle-images-at-non-zero-polar-angles">
<h2>The problem of warping single Ek-vs-angle images at non-zero polar angles<a class="headerlink" href="#the-problem-of-warping-single-ek-vs-angle-images-at-non-zero-polar-angles" title="Link to this heading"></a></h2>
<p>If a 2D measurement (ie. a single angle versus energy spectrum) has been taken with an off-normal polar angle, k-warping is a problem. Strictly speaking, proper k-warping of such an image is not possible unless it is part of a 3D source matrix (i.e. a manipulator polar scan or a deflector scan). As we’ll see, you can often get away with it anyway.</p>
<p>The reason for this is that you are hoping that your plane cut through (<span class="math notranslate nohighlight">\(\Theta_X\)</span> , <span class="math notranslate nohighlight">\(\Theta_Y\)</span> , <span class="math notranslate nohighlight">\(E_K\)</span>) space will map to a vertical cut through (<span class="math notranslate nohighlight">\(k_X\)</span> , <span class="math notranslate nohighlight">\(k_Y\)</span> , <span class="math notranslate nohighlight">\(E_K\)</span>) space. But it doesn’t, due to the ‘keystone warping’ effect discussed above. The effect will be worst at low photon energies and when measuring over a large Ek range. But depending on what you are using the measurement for, the error might be an acceptable tradeoff for the gain in measurement efficiency.</p>
<p>In <em>pesto</em> you can k-warp such images in two ways:</p>
<ol class="arabic simple">
<li><p>Measure and k-warp a 3D matrix, then extract an arbitrary slice from it (error-free warping)</p></li>
<li><p>Just go ahead and warp a single 2D source image, but if beQuiet=False it will print a warning about the error introduced.</p></li>
</ol>
</section>
</section>


           </div>
          </div>
          <footer>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright None.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>