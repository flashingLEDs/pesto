Example data from Bloch
=================================

While *pesto* outwardly presents only a single function to k-warp data (i.e. to map angles to wavevectors), behind the scenes it is looking at what you have passed in and choosing one of four different k-warping functions to apply. This page contains an explanation of what is going on in each of them.

Simple case
------------

The simplest case is one where the analyzer slit is centered at normal emission, i.e. no polar or tilt offset. The analyzer records electrons emitted within the plane of the analyzer slit, and tells us the angle :math:`\alpha` at which they were emitted. 

The in-plane component of the electron wavevector, at least in the direction of the analyzer slit (:math:`k_Y`) can be determined from such a measurement:

.. image:: /_static/pesto/kwarp_geometry.png
	:align: center

.. math::	
	k_Y = |\vec{k}| \sin(\alpha)

.. math::	
	E_K = |\vec{p}|^2/2m_e

.. math::	
	|\vec{p}| = \hbar |\vec{k}|

Combining, we get:

.. math::	
	k_Y = 1/\hbar  \sqrt{2  m_e  E_k}  \sin(\alpha) 

		= 0.512 \sqrt{E_k}  \sin(\alpha) 


The magic number 0.512 comes from the term :math:`\frac{\sqrt{2 m_e}}{ \hbar}`:

::

	eV_per_J = 1.602176e-19
	hbar__Js = 1.054571e-34
	electron_mass__kg = 9.1093e-31
	angstroms_per_meter=1e-10

	prefactor = (math.sqrt(2*electron_mass__kg*eV_per_J)/(hbar__Js))*angstroms_per_meter # = 0.51234


Things remain simple if we introduce a tilt angle offset :math:`\xi` to the emission angle :math:`\alpha`:

.. math::
	k_Y = 0.512 \sqrt{E_k} \sin(\alpha + \xi)

.. note::
	An important observation is that k-warping single :math:`E_K`-vs-:math:`\alpha` images has the effect of keystone distorting the image - i.e. squashing it at the bottom. A given angle range spans a smaller k range at lower kinetic energies. The origin of this is the  :math:`\sqrt{E_K}` term in the total photoelectron momentum  :math:`|\vec{k}|`. As a consequence, the effect is more obvious when measuring lower kinetic energies and when spanning a large :math:`E_K` range.

Within *pesto*, this operation on 2D images is handled by the internal function ``kwarp__energyAngleImage()``. It performs a reverse interpolation: construct the energy-k grid we want to have, and then point-by-point look up the intensity of the corresponding pixel(s) in the source energy-angle image.

Photon energy scan 3D datasets are k-warped in the same way, by the internal *pesto* function ``kwarp__wholeMatrix_hvscan_warp()``. This simple treatment is possible because at least so far *pesto* only warps the analyzer angle axis, it does not warp :math:`h\nu` to :math:`k_Z`

Since the slit is centered at normal emission, the detected electrons will have no component in the perpendicular (:math:`k_X`) direction.



Warping with a polar angle or deflection offset 
--------------------------------------------------------------

If we have rotated the sample in the polar direction (perpendicular to the analyzer slit) away from normal emission, OR deflected off-normal electrons into the analyzer, we now have the problem that electrons detected by the analyzer will have both :math:`k_X` and :math:`k_Y` components. This is treatable with some heavy trigonometry - for details, see  `Ishida and Shin  <https://doi.org/10.1063/1.5007226>`_ (Rev. Sci. Inst. **89** 043903 2018). Their 'Type I' and 'Type I+deflector' systems match the geometry at Bloch, but we slightly alter their equations in the following ways.

The geometry at Bloch, applicable for both endstations, is shown below:

.. image:: /_static/pesto/blochGeometry.png
	:align: center

We choose to have a system in which positive analyzer slit angles correspond to electrons with positive :math:`k_Y`, and going to positive manipulator polar angles probes electrons with positive :math:`k_X`. On the A-endstation, going to negative tilt angles probes electrons with more negative :math:`k_Y` values, while going to positive deflector angles probes electrons with positive :math:`k_X`.

In the coordinate system described by Ishida and Shin, if we set a constant azimuthal angle (:math:`\delta`) of :math:`\pi/2` then we achieve almost all of this, we are just left with positive analyzer angles mapping to negative :math:`k_Y` values. This is addressed by inverting :math:`\alpha` whenever it appears.

Setting this fixed azimuth also sets all :math:`\sin(\delta)` terms to 1 and all :math:`\cos(\delta)` terms to 0, considerably simplifying the warping equations and speeding up the calculations. While it removes the ability to rotate the data *during* k-warping, this can be handled independently after performing k-warping if required.

Finally, Ishida and Shin redefine :math:`\beta` between the manipulator (:math:`f_I`) and deflector (:math:`f_I'`) cases. For consistency we choose :math:`\beta` to always mean deflection perpendicular to the analyzer slit, and :math:`\chi` to always mean the sample polar angle offset.

With all that said, let:

:math:`\chi` = polar angle offset of the sample (perpendicular to the analyzer slit)

:math:`\xi` = tilt angle offset of the sample (along the analyzer slit)

:math:`\alpha` = emission angle recorded by the analyzer, along the slit

:math:`\beta` = emission angle recorded by the analyzer when deflecting, perpendicular to the slit

**Mechanical** polar-scan 3D datasets are k-warped by the internal *pesto* function ``kwarp__wholeMatrix_kwarp()``, and use the following equations for forward and reverse mapping:


.. math::
	k_X = C \left(\sin(\chi)\cos(-\alpha)\right)

.. math::
	k_Y = C \left(\cos(-\alpha)\sin(\xi)\cos(\chi) - \cos(\xi)\sin(-\alpha)\right)

.. math::
	\alpha = -\textrm{asin}\left(\left(\sin(\xi) E - \cos(\xi)k_Y  \right)/C\right)

.. math::
	\chi = \textrm{atan}\left((k_X) / \left(\sin(\xi)k_Y + \cos(\xi)\left( E \right)\right)\right)

where:

:math:`C = (0.512316\sqrt{E_K})` 

:math:`E = \sqrt{C^2 - k_X^2 - k_Y^2}` 

Corresponding functions in *pesto*: ``angle_to_kx_manipulator()``, ``angle_to_ky_manipulator()``, ``k_to_polar_manipulator()``


**Deflector-scan** 3D datasets are performed by ``kwarp__wholeMatrix_kwarp_deflector()``, and use the following horrific equations for forward and reverse mapping:

.. math::
	k_X = C \left(\beta\cos(\chi)\frac{\sin(\sqrt{\alpha^2 + \beta^2})}{\sqrt{\alpha^2 + \beta^2}} +   \sin(\chi)\cos(\sqrt{\alpha^2 + \beta^2}) \right)


.. math::
	k_Y = C \left(\left(\alpha\cos(\xi) - \beta\sin(\xi)\sin(\chi) \right)\frac{\sin(\sqrt{\alpha^2 + \beta^2})}{\sqrt{\alpha^2 + \beta^2}} +   \left(\sin(\xi)\cos(\chi)\right)\cos(\sqrt{\alpha^2 + \beta^2}) \right)



.. math::
	\alpha = \frac{\textrm{acos}\left(\left(\sin(\chi)k_X + \cos(\chi)\sin(\xi)k_y + \cos(\chi)\cos(\xi)E\right)/C \right) \\ \times \left(\cos(\xi)k_Y - \sin(\xi)E\right)}{\sqrt{C^2 - \left(\sin(\chi)k_x + \cos(\chi)\sin(\xi)k_y + \cos(\chi)\cos(\xi)E \right)^2}}

.. math::
	\beta = -\frac{\textrm{acos}\left(\left(\sin(\chi)k_X + \cos(\chi)\sin(\xi)k_y + \cos(\chi)\cos(\xi)E\right)/C \right) \\ \times \left(-\cos(\chi)k_x + \sin(\chi)\sin(\xi)k_Y + \sin(\chi)\cos(\xi)E\right)}{\sqrt{C^2 - \left(\sin(\chi)k_x + \cos(\chi)\sin(\xi)k_y + \cos(\chi)\cos(\xi)E \right)^2}}




Corresponding functions in *pesto*: ``None``, ``angle_to_ky_deflector()``, ``k_to_alpha_deflector()``, ``k_to_beta_deflector()``


----

As a quick sanity check, notice that in both the deflector and manipulator scan cases, if we set polar :math:`\chi` and deflection :math:`\beta` angles to zero, the expression for :math:`k_Y` reduces to:

.. math::
	k_Y = 0.512\sqrt{E_K} \left(\cos(-\alpha)\sin(\xi) - \cos(\xi)\sin(-\alpha)\right)

.. math::
	k_Y = 0.512\sqrt{E_K} \left(\cos(\alpha)\sin(\xi) + \cos(\xi)\sin(\alpha)\right)


which by a trig identity reduces to the simple equation we were using earlier:

.. math::
	k_Y = 0.512 \sqrt{E_k} \sin(\alpha + \xi)


Why are different equations necessary for mechanical vs deflection ARPES maps?
------------------------------------------------------------------------------------

You can acquire a constant energy surface by either scanning :math:`\beta` in a deflector measurement or scanning :math:`\chi` in a mechanical polar scan. The k-warping of the datasets diverges whenever **both** :math:`\alpha` and :math:`\beta` in the deflector measurement are non-zero. This is because there are different coordinate systems involved in each case. Conventional ARPES mapping employs a *tilt-angular* coordinate system, see (b) below, like latitude and longitude in geography. Deflector schemes employ a different scheme called *tilt-polar*, see (a) below (figure reproduced from Ishida and Shin). What we are calling :math:`\alpha` and :math:`\beta` for the deflector equations correspond to :math:`\Theta_X` and :math:`\Theta_Y` in this figure.

.. image:: /_static/pesto/kwarp_coordinatesystems.png
	:align: center

The tilt-angular coordinate system has the property that iso-tilt lines get 'squeezed together' at the poles (or if you prefer, latitude spacing decreases at high longitudes). The same is not true in the :math:`\Theta_X`-:math:`\Theta_Y` system. In practice, with mainstream hemispherical analysers the error introduced by using the wrong k-warping equations is quite small, 1-2% at most. This is because the differences only grow pronounced at large angles, which are never reached when the angular acceptance is limited to around |plusminus| 15\ |deg|. 


The problem of warping single Ek-vs-angle images at non-zero polar angles
-------------------------------------------------------------------
If a 2D measurement (ie. a single angle versus energy spectrum) has been taken with an off-normal polar angle, k-warping is a problem. Strictly speaking, proper k-warping of such an image is not possible unless it is part of a 3D source matrix (i.e. a manipulator polar scan or a deflector scan). As we'll see, you can often get away with it anyway. 

The reason for this is that you are hoping that your plane cut through (:math:`\Theta_X` , :math:`\Theta_Y` , :math:`E_K`) space will map to a vertical cut through (:math:`k_X` , :math:`k_Y` , :math:`E_K`) space. But it doesn't, due to the 'keystone warping' effect discussed above. The effect will be worst at low photon energies and when measuring over a large Ek range. But depending on what you are using the measurement for, the error might be an acceptable tradeoff for the gain in measurement efficiency.

In *pesto* you can k-warp such images in two ways:

1. Measure and k-warp a 3D matrix, then extract an arbitrary slice from it (error-free warping)

2. Just go ahead and warp a single 2D source image, but if beQuiet=False it will print a warning about the error introduced.


.. |deg|    unicode:: U+00B0
.. |plusminus|    unicode:: U+00b1

