pesto
=================================

What is this?
--------------------

**pesto** (**P**\ hoto\ **e**\ mission **s**\ pectroscopy **to**\ ols) is an open-source python library, designed to be used in a Jupyter notebook environment, for planning, visualizing and analyzing ARPES and XPS measurements taken at the Bloch beamline at MAX-IV. Maybe from other laboratories too! But for now it's still very much aimed at Bloch. 

Confused? **Python** (1991-) is a programming language. **Jupyter** (2014-) is a spin-off from **IPython** (2001-), that provides a *notebook interface* for Python. A notebook interface is something like a blend of a word processor and a programming shell. In one infinitely scrolling browser page, you can switch between text to explain what you are doing, and python code execution. You might have encountered the notebook concept already in Maple or Mathematica - Mathematica invented the concept in 1988.


Examples
--------------------

There are two levels of interaction you can have with *pesto*. The simplest is to use only basic interactive tools to help you conduct a beamtime, such as calculators or immediate on-site vizualization:

.. image:: /_static/pesto/resolutionCalculator.png
  :align: center
|
.. image:: /_static/pesto/explorer_example.png
  :align: center
|
.. image:: /_static/pesto/explorer_example2.png
  :align: center
|

But if you're comfortable playing around with matplotlib syntax, you can also use it for more extensive analysis and plotting tasks:

.. image:: /_static/pesto/fancy_ek_plot.png
  :align: center
|

For more examples, see the 'Code Snippets' page


Why does this exist?
--------------------

For the kind of work carried out at Bloch and other photoemission beamlines, common analysis software packages are Igor Pro, Origin, Python and Matlab. Fitting of XPS data might additionally involve CasaXPS, while STM images might use SPIP, Gwyddion or WSxM. Powerpoint is often used to construct intermediate summaries of the measurements in a digestable, logically arranged document.

The *pesto* library is an attempt to solve several commonly encountered problems:

- These programs offer the building blocks you need to create sophisticated analysis routines, but you still need to either create the higher level scripts/macros yourself, or hope to obtain a copy of someone else's scripts to accomplish what you need. As a beamline we must be able to offer tools to those users who don't yet have a solution.

- Except for Gwyddion, WSxM and Python, these software packages are not free. It's a problem if the tools we offer require a software licence that our users might not have.

- Igor in particular suffers from a window management nightmare when analysis starts to span more than a dozen plots. The notebook concept of an infinitely scrolling page with table of contents often makes it easier to keep track of results from a long experimental run.

- For many of these packages, notably Igor and Origin, the *default* mode of interaction is graphical and exploratory and does not do very well at keeping a record of what has been done. This can become very time consuming if you need to repeat your work. The alternative approach familiar to Matlab & Python users is one in which your analysis is a list of instructions that can automatically transform raw data to the final result. While this presents more friction when initally exploring data, when it comes to data analysis it is a powerful concept that's worth investing in, both for your mental health and for the reliability of your analysis.

Aren't there other libraries doing this already?
-------------------------------------------------
Yes! Before you get too invested in any of this, you might want to also take a look at other ARPES tools out there, including but not limited to:

`PyARPES <https://arpes.readthedocs.io/en/latest/index.html>`_ from Conrad Stansbury

`NavARP <https://fbisti.gitlab.io/navarp/index.html>`_ from Federico Bisti

`piva <https://piva.readthedocs.io/en/latest/>`_ from Wojtek Pudelko

`ARPYS <https://github.com/kuadrat/arpys>`_ from Kevin Kramer

`ARPES Python Tools <https://pranabdas.github.io/arpespythontools/docs/>`_ from Pranab Das


.. toctree::
   :caption: pesto
   :hidden:
   
   pesto_settingup
   pesto_preliminarynotes
   pesto_bloch
   pesto_core
   pesto_export
   pesto_spin
   pesto_XPS
   pesto_DFT
   snippets
   kwarping



