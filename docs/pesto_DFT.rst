DFT module
=================================

This helper module is specific to loading and plotting the output of certain Quantum Espresso calculations. Specifically, it can handle slab bandstructure calculations (including spin projections) and 'surface projected bulk bandstructure' calculations, in which a bulk bandstructure calculation is repeated many times at different kz values.

The output must follow a specific naming convention to be loadable. In the DFT example data folders you will also find the input files used to generate the output.

DFT.loadBands
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Load the output of a slab bandstructure calculation into a pesto spectrum. 

It will automatically look for additional spin-resolved calculation output (i.e. if the three lsigma flags were set on the bands.x input), and load this information if it finds it.

::

  def DFT.loadBands(	folderPath,
  			bandsOutputFileName,
  			nscfInputFileName,
  			nscfOutputFileName,
  			scfOutputFileName,
  			invertEnergyAxis,
  			beQuiet)

**Required parameters**

  ``folderPath``: Path to the **folder** containing the output files.


**Optional parameters:**
  ``bandsOutputFileName``: The assumed filename for the output band file is "bandstructure.out.gnu". If the name is different, provide it here as a string.

  ``scfOutputFileName``: The loader will want to examine the scf output file. By default it assumes this is named "scf.out". If the name is different, provide it here as a string.

  ``nscfInputFileName``: The loader will want to examine the nscf input file. By default it assumes this is named "bands.in". If the name is different, provide it here as a string.

  ``nscfOutputFileName``: The loader will want to examine the nscf output file. By default it assumes this is named "bands.out". If the name is different, provide it here as a string.

  ``invertEnergyAxis``: If True, invert the energy axis. By default, Eb is positive about Ef. When doing overlays on experimental measurements, we would prefer if Eb were positive below Ef. 

 ``beQuiet``: If False, prints information about what it's doing (default = False)


**Returns:**
  A pesto 'spectrum' (Dictionary object)

**Example usage:**

::

	fig,ax = plt.subplots(figsize=(11,4.3))

	GK,GM,KM = 1.829,1.584,0.915
	kPath = [["$K$",-(KM+GM)],["$M$",-(GM)],["$\Gamma$",(0)],["$K$",(GK)]]

	bulkProj = pesto.DFT.loadProjectedBulkBands("example_data/DFT_bulkproj",beQuiet=True)
	pesto.quickPlot(bulkProj,axis=ax,kPath=kPath,filled=True,color='gray')

	slabBands = pesto.DFT.loadBands("example_data/DFT_slab/",beQuiet=True)
	pesto.quickPlot(slabBands,axis=ax,kPath=kPath,lw=2,drawHighSymmetryLabels=True)

	ax.set_ylim([-3.8,0.2])
	plt.show()


.. image:: /_static/pesto/dft_slabBands.png
  :align: center
|

::

	fig,axes = plt.subplots(figsize=(7,8),nrows=2)

	GK,KM,GM = 1.359,0.680,1.177
	kPath=[["$M$",-GM],["$\Gamma$",0],["$K$",GK]]

	slabBands = pesto.DFT.loadBands(folderPath="example_data/DFT_spinslab/")
	ax=axes[0]
	pesto.quickPlot(slabBands,axis=ax,kPath=kPath)
	ax.set_ylim([-1,0.3])
	ax.set_xlim([-0.6,0.6])

	ax=axes[1]
	pesto.quickPlot(slabBands,axis=ax,kPath=kPath,spinProjection=1,bandIndicesToPlot=range(252,256))
	ax.set_ylim([-1,0.3])
	ax.set_xlim([-0.6,0.6])
	ax.set_title("Px projection")

	plt.tight_layout()
	plt.show()


.. image:: /_static/pesto/dft_spinProj.png
  :align: center
|

DFT.loadProjectedBulkBands
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
When pointed to a folder containing a 'projected bulk bandstructure' calculation (consisting of many output files at different kz values), loads everything into a pesto spectrum.

::

  def DFT.loadProjectedBulkBands(	folderPath,
  					bandsOutputPrefix,
  					scfOutputFileName,
  					nscfInputFileName,
  					nscfOutputFileName,
  					invertEnergyAxis,
  					beQuiet)

**Required parameters**

  ``folderPath``: Path to the **folder** containing the output files.


**Optional parameters:**
  ``bandsOutputPrefix``: The assumed filename for the output band files are "[number].out.gnu". A filePrefix string can be passed in if the output files are rather named "[filePrefix][number].out.gnu". Default = "", i.e. empty string

  ``scfOutputFileName``: The loader will want to examine the scf output file. By default it assumes this is named "scf.out". If the name is different, provide it here as a string.

  ``nscfInputFileName``: The loader will want to examine the nscf calculation input  file. By default it assumes this is named "bands.in". If the name is different, provide it here as a string..

  ``nscfOutputFileName``: The loader will want to examine the nscf output file. By default it assumes this is named "scf.out". If the name is different, provide it here as a string..

 ``beQuiet``: If False, prints information about what it's doing (default = False)

**Returns:**
  A pesto 'spectrum' (Dictionary object)


**Example usage:**

::

	fig,ax = plt.subplots(figsize=(11,4.3))

	bulkProj = pesto.DFT.loadProjectedBulkBands("example_data/DFT_bulkproj")
	pesto.quickPlot(bulkProj,axis=ax)
	ax.set_ylim([-3.8,0.2])
	plt.show()

.. image:: /_static/pesto/dft_loadProjectedBulkBands.png
  :align: center
|