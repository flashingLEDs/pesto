Bloch-specific calculators
======================================

temperatureCalculator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Estimate the settling time for the true sample temperature given a step-increase in nominal temperature of the Carving manipulator.

::

  def temperatureCalculator(startT,endT)


**Required parameters**

  ``startT``: Starting temperature in Kelvin

  ``endT``: Final temperature in Kelvin


**Example:**

.. image:: /_static/pesto/temperatureCalculator.png
  :align: left
|

------------

resolutionCalculator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
An interactive calculator for the total energy resolution

::

  def resolutionCalculator()


**Example:**

.. image:: /_static/pesto/resolutionCalculator.png
  :align: left
|

------------


