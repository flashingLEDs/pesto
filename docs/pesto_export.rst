Spectrum export functions
=================================

Note: These functions concern getting data out of memory to be used with different software. If you just want to export vector or bitmap images of graphs produced by pesto, this can be done using the matplotlib 'savefig' command.


export.txt
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Exports a 1D spectrum (e.g. EDC or spin polarization) to a plain text file, suitable for loading into other software. Higher dimensional datasets are currently not supported. Metadata is not included.

::

  def txt(spectrum,fileName)

**Required parameters**

  ``spectrum``: The pesto spectrum to export. Must be 1D.


  ``fileName``: The name of the file to export it to. If you provide an already-existing file, it will be overwritten. If the file does not already exist, it will be created.

**Returns:**
  Nothing (saves to file)

|


------------

export.nxs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

(In development) Export any pesto spectrum to an hdf5 file that conforms to the NeXus data format.

::

  def nxs(spectrum,fileName)

**Required parameters**

  ``spectrum``: The pesto spectrum to export


  ``fileName``: The name of the file to export it to. If you provide an already-existing file, it will be overwritten. If the file does not already exist, it will be created.

**Returns:**
  Nothing (saves to file)

|


------------




